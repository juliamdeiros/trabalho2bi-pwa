﻿using Buffet.Models.Buffet.Cliente;
using Buffet.Models.Buffet.Convidado;
using Buffet.Models.Buffet.Evento;
using Buffet.Models.Usuario;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Buffet.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<CategoriaEntity> Categoria { get; set; }
        public DbSet<ItemEntity> Item { get; set; }
        public DbSet<ParametroEntity> Parametro { get; set; }
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
    }
}
