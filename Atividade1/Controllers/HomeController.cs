﻿using Atividade1.Models;
using Buffet.Acesso;
using Buffet.Database;
using Buffet.Models.Buffet.Cliente;
using Buffet.Models.Buffet.Convidado;
using Buffet.Models.Buffet.Evento;
using Buffet.Models.Usuario;
using Buffet.RequestModel;
using Buffet.RequestModel.Cliente;
using Buffet.RequestModel.Evento;
using Buffet.ViewModels.Cliente;
using Buffet.ViewModels.Convidado;
using Buffet.ViewModels.Evento;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Atividade1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DatabaseContext _databaseContext;

        private Usuario _usuario;
        private readonly CadastroUsuarioService _cadastroUsuarioService;
        private readonly UsuarioService _usuarioService;

        private readonly AcessoService _acessoService;

        private readonly ClienteService _clienteService;
        private readonly TipoClienteService _tipoClienteService;

<<<<<<< HEAD
        private readonly ParametroService _eventoService;
        private readonly SituacaoEvento _situacaoEvento;

        private readonly ItemService _convidadoService;
=======
        private AcessoRegistro historicologin;
>>>>>>> origin/master

        public HomeController(CadastroUsuarioService cadastroUsuarioService)
        {
            _cadastroUsuarioService = cadastroUsuarioService;
        }

        public IActionResult Index()
        {
            //var listaClientes = _databaseContext.Cliente.ToList();

            //_databaseContext.[TABELA].Add([OBJETO]);  -> adicionar no banco
            //_databaseContext.SaveChanges();  -> salva as alterações no banco


            return View();
        }

        public IActionResult Privacidade()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Cadastro()
        {
            var viewmodel = new MsgAcessoViewModel();

            viewmodel.Mensagem = (string)TempData["msg-cadastro"];

            return View(viewmodel);
        }
        [HttpPost]
        public async System.Threading.Tasks.Task<RedirectResult> Cadastro(CadastroRequest request)
        {
            var redirectUrl = "/Home/Cadastro";
            var user = request.User;
            var senha = request.Senha;

            if (user == null)
            {
                TempData["msg-cadastro"] = "O campo de Usuário não pode ficar vazio.";
                return Redirect(redirectUrl);
            }
            if (senha == null)
            {
                TempData["msg-cadastro"] = "O campo de Senha não pode ficar vazio.";
                return Redirect(redirectUrl);
            }

            try
            {
                await _cadastroUsuarioService.RegistrarUsuario(user, senha);
                TempData["msg-cadastro"] = "Cadastro realizado com sucesso!";
                return Redirect("/Home/Login");
            }
            catch (Exception exception)
            {
                TempData["msg-cadastro"] = exception.Message;
                return Redirect("/Home/Cadastro");
            }

            return Redirect(redirectUrl);
        }

<<<<<<< HEAD
        public ICollection<Usuario> ObterUsuarios()
        {
            List<Usuario> usuarios = _databaseContext.Usuario.Include(u => u).ToList(); ;
            return usuarios;
        }

        public RedirectResult Login(LoginRequest request)
=======
        public IActionResult Login(LoginRequest request)
>>>>>>> origin/master
        {
            var usuarios = _usuarioService.ObterTodos();

            Usuario usuarioValidado = null;
            var user = request.User;
            var senha = request.Senha;

            foreach (var usuario in usuarios)
            {
                if (usuario.Login == user && usuario.Senha == senha)
                {
                    usuarioValidado = usuario;
                }                  
            }

<<<<<<< HEAD
            if (usuarioValidado == true)
                return Redirect("/Home/Principal");
=======
            if (usuarioValidado != null)
            {
                DateTime horario = DateTime.Now;
                AcessoRegistro acessoRegistro = new AcessoRegistro(usuarioValidado.Id, horario);
                return View(Painel(usuarioValidado));
            }
>>>>>>> origin/master
            else
            {
                TempData["msg-cadastro"] = "Usuário ou senha incorretos";
                return Redirect("/Home/Login");
            }
        }

        public IActionResult Recuperar()
        {
            return View();
        }
        public IActionResult Termos()
        {
            return View();
        }
        public IActionResult Ajuda()
        {
            return View();
        }
        public IActionResult Painel(Usuario usuarioValidado) 
        {
            var usuario = _acessoService.GetUser(usuarioValidado);

            return View(usuarioValidado);
        }
        public IActionResult Principal()
        {
            return View();
        }
        public IActionResult TermosInterno()
        {
            return View();
        }
        public IActionResult PrivacidadeInterno()
        {
            return View();
        }
        public IActionResult Cliente()
        {
            //cria view model para conter a lista de dados a ser exibido ao usuario
            var viewModel = new CategoriaViewModel()
            {
                MensagemSucesso = (string)TempData["formMensagemSucesso"],
                MensagemErro = (string)TempData["formMensagemErro"]
            };

            //recebe lista de clientes
            var listaDeCliente = _clienteService.ObterTodos();

            //mostra apenas as informações necessárias na view model
            foreach (Cliente clienteEntity in listaDeCliente)
            {
                viewModel.Categoria.Add(new Categoria()
                {
                    Id = clienteEntity.Id.ToString(),
                    cpf = clienteEntity.CPF.ToString(),
                    Nome = clienteEntity.Nome.ToString(),
                    data = clienteEntity.DataNascimento.ToString(),
                    endereco = clienteEntity.Endereco.ToString(),
                    email = clienteEntity.Email.ToString(),
                    observacao = clienteEntity.Observacoes.ToString()
                });
            }
            //retorna a view junto c a view model
            return View(viewModel);
        }
        [HttpGet]
        public IActionResult CadastraCliente()
        {
            var viewModel = new CadastrarCategoriaViewModel()
            {
                FormMensagensErro = (string[])TempData["formMensagensErro"]
            };

            var tipo = _tipoClienteService.ObterTodos(); //tem que criar uma lista no html para escolher o tipo de cliente!!
            foreach (var tipoCliente in tipo)
            {
                viewModel.TipoCliente.Add(new SelectListItem()
                {
                    Value = tipoCliente.Id.ToString(),
                    Text = tipoCliente.Descricao
                });
            }
            return View(viewModel);
        }
        [HttpPost]
        public RedirectToActionResult CadastraCliente(CadastrarCategoriaRequestModel requestModel)
        {
            //validar e filtrar dados
            var listaDeErros = requestModel.ValidarEFiltrar();
            if (listaDeErros.Count > 0)
            {
                TempData["formMensagensErro"] = listaDeErros;
                return RedirectToAction("CadastraCliente");
            }
            //execução
            try
            {
                _clienteService.Cadastrar(requestModel);
                TempData["formMensagemSucesso"] = "Cliente adicionado com sucesso!";
                return RedirectToAction("Cliente");
            }
            catch (Exception exception)
            {
                TempData["formMensagensErro"] = new List<string> { exception.Message };
                return RedirectToAction("CadastraCliente");
            }
        }

        [HttpGet]
        public IActionResult EditaCliente(Guid param)
        {
            //obter a cliente a ser editado
            try
            {
                var clienteEntity = _clienteService.ObterPorID(param);
                var viewModel = new EditarCategoriaViewModel()
                {
                    Id = clienteEntity.Id.ToString(),
                    cpf = clienteEntity.CPF.ToString(),
                    Nome = clienteEntity.Nome.ToString(),
                    data = clienteEntity.DataNascimento.ToString(),
                    endereco = clienteEntity.Endereco.ToString(),
                    email = clienteEntity.Email.ToString(),
                    observacao = clienteEntity.Observacoes.ToString()
                };
                return View();
            }
            catch (Exception e)
            {
                TempData["formMensagemErro"] = e.Message;
                return RedirectToAction("EditaCliente");
            }
        }

        [HttpPost]
        public RedirectToActionResult EditaCliente(Guid param, CadastrarCategoriaRequestModel requestModel)
        {
            var listaDeErros = requestModel.ValidarEFiltrar();
            if (listaDeErros.Count > 0)
            {
                TempData["formMensagensErro"] = listaDeErros;
                return RedirectToAction("EditaCliente");
            }

            try
            {
                _clienteService.Editar(param, requestModel);
                TempData["formMensagemSucesso"] = "Cliente editadi com sucesso!";
                return RedirectToAction("Cliente");
            }
            catch (Exception e)
            {
                TempData["formMensagensErro"] = new List<string> { e.Message };
                return RedirectToAction("EditaCliente");
            }
        }

        [HttpGet]
        public IActionResult RemoveCliente(Guid param)
        {
            //obter entidade
            try
            {
                var clienteEntity = _clienteService.ObterPorID(param);
                var viewModel = new RemoverItemViewModel()
                {
                    Id = clienteEntity.Id.ToString(),
                    cpf = clienteEntity.CPF.ToString(),
                    Nome = clienteEntity.Nome.ToString(),
                    data = clienteEntity.DataNascimento.ToString(),
                    endereco = clienteEntity.Endereco.ToString(),
                    email = clienteEntity.Email.ToString(),
                    observacao = clienteEntity.Observacoes.ToString()
                };
                return View(viewModel);
            }
            catch (Exception e)
            {
                TempData["formMensagensErro"] = e.Message;
                return RedirectToAction("Cliente");
            }
        }

        [HttpPost]
        public RedirectToActionResult RemoveCliente(Guid param, object requestModel)
        {
            try
            {
                _clienteService.Remover(param);
                TempData["formMensagensErro"] = "Cliente removido com sucesso!";

                return RedirectToAction("Cliente");
            }
            catch (Exception e)
            {
                TempData["formMensagensErro"] = new List<string> { e.Message };
                return RedirectToAction("Remover");
            }
        }
        public IActionResult Evento()
        {
            var viewModel = new ParametroViewModel()
            {
                MensagemSucesso = (string)TempData["formMensagemSucesso"],
                MensagemErro = (string)TempData["formMensagemErro"]
            };

            var listaDeEventos = _eventoService.ObterTodos();

            foreach (ParametroEntity eventoEntity in listaDeEventos)
            {
                viewModel.Eventos.Add(new Evento()
                {
                    Id = eventoEntity.Id.ToString(),
                    tipo = eventoEntity.Tipo.Desricao,
                    Descricao = eventoEntity.Descricao,
                    Inicio = eventoEntity.Inicio.ToString(),
                    Termino = eventoEntity.Termino.ToString(),
                    Local = eventoEntity.Local,
                    Endereco = eventoEntity.Endereco,
                    Observacoes = eventoEntity.Observacoes
                });
            }
            return View(viewModel);
        }
        [HttpGet]
        public IActionResult CadastraEvento()
        {
            var viewModel = new CadastraParametroViewModel()
            {
                FormMensagensErro = (string[])TempData["formMensagensErro"]
            };
            var tipos = _tipoClienteService.ObterTodos();
            foreach (var tipoEntity in tipos)
            {
                viewModel.Tipo.Add(new SelectListItem()
                {
                    Value = tipoEntity.Id.ToString(),
                    Text = tipoEntity.Descricao
                });
            }
            return View(viewModel);
        }

        [HttpPost]
        public RedirectToActionResult CadastraEvento(Buffet.RequestModel.Evento.CadastrarItemRequestModel requestModel)
        {
            var listaDeErros = requestModel.ValidarEFiltrar();
            if (listaDeErros.Count > 0)
            {
                TempData["FormMensagensErro"] = listaDeErros;
                return RedirectToAction("CadastraEvento");
            }

            try
            {
                _eventoService.Cadastrar(requestModel);
                TempData["formMensagemSucesso"] = "Evento adicionado com sucesso!";
                return RedirectToAction("Evento");
            }
            catch (Exception e)
            {
                TempData["formMensagensErro"] = new List<string> { e.Message };
                return RedirectToAction("CadastraEvento");
            }
        }

        public IActionResult EditaEvento(Guid param)
        {
            try
            {
                var eventoEntity = _eventoService.ObterPorID(param);
                var viewModel = new EditarParametroViewModel()
                {
                    Id = eventoEntity.Id.ToString(),
                    Descricao = eventoEntity.Descricao,
                    Endereco = eventoEntity.Endereco,
                    Inicio = eventoEntity.Inicio.ToString(),
                    Termino = eventoEntity.Termino.ToString(),
                    Local = eventoEntity.Local,
                    Observacoes = eventoEntity.Observacoes
                };

                var tipo = _tipoClienteService.ObterTodos();
                foreach (var tipoEntity in tipo)
                {
                    viewModel.Tipo.Add(new SelectListItem()
                    {
                        Value = tipoEntity.Id.ToString(),
                        Text = tipoEntity.Descricao
                    });
                }
                return View(viewModel);
            }
            catch (Exception e)
            {
                TempData["formMensagemErro"] = e.Message;
                return RedirectToAction("EditaEvento");
            }
        }

        [HttpPost]
        public RedirectToActionResult EditaEvento(Guid param, Buffet.RequestModel.Evento.CadastrarItemRequestModel requestModel)
        {
            var listaDeErros = requestModel.ValidarEFiltrar();
            if (listaDeErros.Count > 0)
            {
                TempData["formMensagensErro"] = listaDeErros;
                return RedirectToAction("EditaEvento");
            }
            try
            {
                _eventoService.Editar(param, requestModel);
                TempData["formMensagemSucesso"] = "Evento editado com sucesso!";
                return RedirectToAction("Evento");
            }
            catch (Exception e)
            {
                TempData["formMensagensErro"] = new List<string> { e.Message };
                return RedirectToAction("EditaEvento");
            }
        }

        [HttpGet]
        public IActionResult RemoveEvento(Guid param)
        {
            try
            {
                var eventoEntity = _eventoService.ObterPorID(param);
                var viewModel = new RemoverParametroViewModel()
                {
                    Id = eventoEntity.Id.ToString(),
                    Descricao = eventoEntity.Descricao,
                    Endereco = eventoEntity.Endereco,
                    Inicio = eventoEntity.Inicio.ToString(),
                    Termino = eventoEntity.Termino.ToString(),
                    Local = eventoEntity.Local,
                    Observacoes = eventoEntity.Observacoes
                };
                return View(viewModel);
            }
            catch (Exception e)
            {
                TempData["formMensagemErro"] = e.Message;
                return RedirectToAction("Evento");
            }
        }

        [HttpPost]
        public RedirectToActionResult RemoveEvento(Guid param, object requestModel)
        {
            try
            {
                _eventoService.Remover(param);
                TempData["formMensagemSucesso"] = "Evento removido com sucesso!";
                return RedirectToAction("Evento");
            }
            catch (Exception e)
            {
                TempData["formMensagensErro"] = new List<string> { e.Message };
                return RedirectToAction("RemoveEvento");
            }
        }
        public IActionResult Convidados()
        {
            var viewModel = new ItemViewModel()
            {

            };
            var listaDeConvidado = _convidadoService.ObterPorEvento();

            foreach (ConvidadoEntity convidadoEntity in listaDeConvidado)
            {
                viewModel.convidado.Add(new Convidado()
                {
                    Id = convidadoEntity.Id.ToString(),
                    Nome = convidadoEntity.Nome,
                    DataNascimento = convidadoEntity.DataNascimento.ToString(),
                    Email = convidadoEntity.Email,
                    CPF = convidadoEntity.CPF,
                    Observacoes = convidadoEntity.Observacoes,
                    Situacao = convidadoEntity.Situacao.Descricao
                });
            }
            return View(viewModel);
        }
    }
}
