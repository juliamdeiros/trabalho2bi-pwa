﻿using Buffet.Models.Buffet.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.RequestModel.Cliente
{
    public class CadastrarCategoriaRequestModel : IDadosBasicosCategorias
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
        public string CategoriaPaiId { get; set; }

        public ICollection<string> ValidarEFiltrar()
        {
            var listaDeErros = new List<string>();
            return listaDeErros;
        }
    }
}
