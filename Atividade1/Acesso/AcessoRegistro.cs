﻿using Buffet.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Usuario
{
    public class AcessoRegistro
    {
        private UserManager<Usuario> _userManager;
        private DatabaseContext _databaseContext;
        
        private int UsuarioId;
        private DateTime Horario;

        public AcessoRegistro(int usuarioId, DateTime horario)
        {
            UsuarioId = usuarioId;
            this.Horario = horario;
        }

        public AcessoRegistro(DateTime registro, DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }       

        public Usuario GetUser(Usuario logado)
        {
            var id = logado.Id;
            var login = logado.Login;
            var senha = logado.Senha;

            return logado;
        }

        public static void SalvarAlteracoes(Usuario logado, string login, string senhaAtual, string novaSenha)
        {
            int id = logado.Id;
            _userManager.SetUserNameAsync(logado, login); //Alterar login
            _userManager.ChangePasswordAsync(logado, senhaAtual, novaSenha); //Alterar senha

            _databaseContext.SaveChangesAsync();    
        }
    }
}
