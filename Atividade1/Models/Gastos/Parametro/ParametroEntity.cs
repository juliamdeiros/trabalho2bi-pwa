﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Evento
{
    public class ParametroEntity
    {
        public Guid Id { get; set; }
        public int ValorKwh { get; set; }
        public int FaixaConsumoAlto { get; set; }
        public int FaixaConsumoMedio { get; set; }
    }
}
