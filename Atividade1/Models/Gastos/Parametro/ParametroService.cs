﻿using Buffet.Database;
using Buffet.Models.Buffet.Cliente;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Evento
{
    public class ParametroService
    {
        private readonly DatabaseContext _databaseContext;

        public ParametroService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<ParametroEntity> ObterTodos()
        {
            return _databaseContext.Parametro.Include(e => e).ToList();
        }

        public ParametroEntity Cadastrar(IDadosBasicosParametro dadosBasicos)
        {
            var novoParametro = ValidarDadosBasicos(dadosBasicos);
            _databaseContext.Parametro.Add(novoParametro);
            _databaseContext.SaveChanges();
            return novoParametro;
        }

        private ParametroEntity ValidarDadosBasicos(IDadosBasicosParametro dadosBasicos, ParametroEntity entidadeExistente = null)
        {
            //instanciar entidade
            var entidade = entidadeExistente ?? new ParametroEntity();

            return entidade;
        }

        public ParametroEntity Editar(Guid id, IDadosBasicosParametro dadosBasicos)
        {
            var parametroEntity = ObterPorID(id);
            parametroEntity = ValidarDadosBasicos(dadosBasicos, parametroEntity);
            _databaseContext.SaveChanges();

            return parametroEntity;
        }

        public ParametroEntity Remover(Guid id)
        {
            var parametroEntity = ObterPorID(id);
            _databaseContext.Parametro.Remove(parametroEntity);
            _databaseContext.SaveChanges();

            return parametroEntity;
        }

        public ParametroEntity ObterPorID(Guid id)
        {
            try
            {
                return _databaseContext.Parametro.Include(e => e).First(e => e.Id == id);
            }
            catch
            {
                throw new Exception("Parametro de ID #" + id + "não encontrado");
            }
        }
    }
    public interface IDadosBasicosParametro
    {
        public string Id { get; set; }
        public string ValorKwh { get; set; }
        public string FaixaConsumoAlto { get; set; }
        public string FaixaConsumoMedio { get; set; }
    }
}

