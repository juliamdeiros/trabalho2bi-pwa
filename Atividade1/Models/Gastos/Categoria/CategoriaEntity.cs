﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Cliente
{
    public class CategoriaEntity
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public int CategoriaPaiId { get; set; }

        public CategoriaEntity()
        {
            
        }
    }
}