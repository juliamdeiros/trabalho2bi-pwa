﻿using Buffet.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Cliente
{
    public class CategoriaService
    {
        private readonly DatabaseContext _databaseContext;

        public CategoriaService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<CategoriaEntity> ObterTodos()
        {
            return _databaseContext.Categoria.Include(c => c).ToList();
        }

        public CategoriaEntity Cadastrar(IDadosBasicosCategorias dadosBasicos)
        {
            var novaCategoria = ValidarDadosBasicos(dadosBasicos);
            _databaseContext.Categoria.Add(novaCategoria);
            _databaseContext.SaveChanges();
            return novaCategoria;
        }

        private CategoriaEntity ValidarDadosBasicos(IDadosBasicosCategorias dadosBasicos, CategoriaEntity entidadeExistente = null)
        {
            //instanciar entidade
            var entidade = entidadeExistente ?? new CategoriaEntity();

            return entidade;
        }

        public CategoriaEntity Editar(Guid id, IDadosBasicosCategorias dadosBasicos)
        {
            var categoriaEntity = ObterPorID(id);
            categoriaEntity = ValidarDadosBasicos(dadosBasicos, categoriaEntity);
            _databaseContext.SaveChanges();

            return categoriaEntity;
        }

        public CategoriaEntity Remover(Guid id)
        {
            var categoriaEntity = ObterPorID(id);
            _databaseContext.Categoria.Remove(categoriaEntity);
            _databaseContext.SaveChanges();

            return categoriaEntity;
        }

        public CategoriaEntity ObterPorID(Guid id)
        {
            try
            {
                return _databaseContext.Categoria.Include(c => c).First(c => c.Id == id);
            }
            catch
            {
                throw new Exception("Categoria de ID #" + id + "não encontrado");
            }
        }
    }
    public interface IDadosBasicosCategorias
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
        public string CategoriaPaiId { get; set; }
    }
}
