﻿using Buffet.Database;
using Buffet.Models.Buffet.Convidado;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Cliente
{
    public class ItemService
    {
        private readonly DatabaseContext _databaseContext;

        public ItemService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<ItemEntity> ObterTodos()
        {
            return _databaseContext.Item.Include(c => c).ToList();
        }

        public ItemEntity Cadastrar(IDadosBasicosItem dadosBasicos)
        {
            var novoItem = ValidarDadosBasicos(dadosBasicos);
            _databaseContext.Item.Add(novoItem);
            _databaseContext.SaveChanges();
            return novoItem;
        }

        private ItemEntity ValidarDadosBasicos(IDadosBasicosItem dadosBasicos, ItemEntity entidadeExistente = null)
        {
            //instanciar entidade
            var entidade = entidadeExistente ?? new ItemEntity();

            return entidade;
        }

        public ItemEntity Editar(Guid id, IDadosBasicosItem dadosBasicos)
        {
            var itemEntity = ObterPorID(id);
            itemEntity = ValidarDadosBasicos(dadosBasicos, itemEntity);
            _databaseContext.SaveChanges();

            return itemEntity;
        }

        public ItemEntity Remover(Guid id)
        {
            var itemEntity = ObterPorID(id);
            _databaseContext.Item.Remove(itemEntity);
            _databaseContext.SaveChanges();

            return itemEntity;
        }

        public ItemEntity ObterPorID(Guid id)
        {
            try
            {
                return _databaseContext.Item.Include(c => c).First(c => c.Id == id);
            }
            catch
            {
                throw new Exception("Item de ID #" + id + "não encontrado");
            }
        }
    }
    public interface IDadosBasicosItem
    {
        public string Id { get; set; }
        public string CategoriaId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string DataFabricacao { get; set; }
        public string ConsumoWatts { get; set; }
        public string HorasUsoDiario { get; set; }
    }
}
