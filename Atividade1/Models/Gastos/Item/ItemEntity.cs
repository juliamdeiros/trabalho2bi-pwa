﻿using Buffet.Models.Buffet.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Convidado
{
    public class ItemEntity
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public DateTime DataFabricacao { get; set; }
        public int ConsumoWatts { get; set; }
        public int HorasUsoDiario { get; set; }
        public CategoriaEntity CategoriaId { get; set; }
    }
}
