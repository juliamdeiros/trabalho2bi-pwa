﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Categoria
{
    public class CategoriaViewModel
    {
        public ICollection<Categoria> categoria { get; set; }
        public string MensagemSucesso { get; set; }
        public String MensagemErro { get; set; }

        public CategoriaViewModel()
        {
            categoria = new List<Categoria>();
        }

    }
     public class Categoria
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
        public string CategoriaPaiId { get; set; }
    }
}
