﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Item
{
    public class EditarItemViewModel
    {
        public string[] FormMensagensErro { get; set; }

        public string Id { get; set; }
        public string CategoriaId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string DataFabricacao { get; set; }
        public string ConsumoWatts { get; set; }
        public string HorasUsoDiario { get; set; }

        public EditarItemViewModel()
        {
        }
    }
}
