﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Parametro
{
    public class CadastraParametroViewModel
    {
        public string[] FormMensagensErro { get; set; }

        public CadastraParametroViewModel()
        {
        }
    }
}
