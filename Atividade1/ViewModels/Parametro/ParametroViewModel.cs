﻿using Buffet.Models.Buffet.Evento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Parametro
{
    public class ParametroViewModel
    {
        public ICollection<Parametro> parametro { get; set; }
        public string MensagemSucesso { get; set; }
        public string MensagemErro { get; set; }

        public ParametroViewModel()
        {
            parametro = new List<Parametro>();
        }

    }
    public class Parametro
    {
        public string Id { get; set; }
        public string ValorKwh { get; set; }
        public string FaixaConsumoAlto { get; set; }
        public string FaixaConsumoMedio { get; set; }
    }
}
